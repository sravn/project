//todo: vary the test cases in sudoku--omp.pbs

#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<omp.h>
#include<ctime> 

using namespace std;

//todo: remove this global declarations
static int SIZE; //should be static

// Empty cells in the sudoku board are denoted by 0 
class sudoku{
	private: short board_size; //todo: this should also be made static 
	public:
		 void setsize(int size)
		 {
			 board_size = (short)size;
		 }
		 sudoku() 
		 {
			 board_size = SIZE;
		 }
		 short getsize()
		 {
			 return board_size;
		 }
		 //for dynamic allocation of the matrix
		 short **mat;
		 short **fixed;

};

typedef class sudoku MATRIX;

/*
   void sudoku::setsize(int size)
   {
   board_size = (short)size;
   }
   */
/*
   short sudoku::getsize()
   {
   return board_size;
   }
   */

/*
 * Defining a linked list node structure that acts as an explicit stack of board configurations
 */ 
struct list_mat {
	MATRIX mat;
	short i, j;
	struct list_mat *next;
};

typedef struct list_mat Node;

//todo: try eliminating the need for global variables
Node *head;
Node *tail;
MATRIX solution; //a global matrix that holds solution if found


void findPreFilledCell(MATRIX* mat, short* iptr, short* jptr);
void findNextFreeCell(MATRIX* mat, short* iptr, short* jptr);

//read initial sudoku board configuration from file specified through commandline
MATRIX readSdkFromFile(char *filename) {
	int i,j;  
	MATRIX matrix;  
	int int_input; //to read in from the file

	FILE* inputMatrix = fopen(filename, "rt");

	fscanf(inputMatrix, "%d", &int_input);
	int l = int_input;

	SIZE = l; //input file first contains the dimensions of the sudoku board
	matrix.setsize(SIZE);

	// allocate memory for matrices
	matrix.mat = (short**)malloc((SIZE)*sizeof(short*));  
	for (i=0;i<(SIZE);i++)
		matrix.mat[i] = (short*) malloc ((SIZE)* sizeof (short));

	matrix.fixed = (short**) malloc((SIZE) * sizeof(short*));
	for (i=0;i<(SIZE);i++)
		matrix.fixed[i] = (short*) malloc ((SIZE) * sizeof (short));


	// initializing the fixed matrix
	for (i=0; i<SIZE; i++) {
		for (j=0; j<SIZE; j++) {     
			matrix.fixed[i][j] = 0;
		}
	}

	//reconfiguring fixed matrix according to the filled positions
	for(i = 0; i < SIZE; i++) {
		for(j = 0; j < SIZE; j++){
			fscanf(inputMatrix, "%d", &int_input);
			matrix.mat[i][j] = int_input;
			if (matrix.mat[i][j] != 0)
				matrix.fixed[i][j] = 1;
		}  
	}

	fclose(inputMatrix);
	return matrix;
}

//prints a MATRIX to the standard output
void printMatrix(MATRIX *matrix) {
	for (int i = 0; i < SIZE; i++) {
		for (int j = 0; j < SIZE; j++) {
			cout<< matrix->mat[i][j]<<" ";
		}
		cout<<"\n";
	}
}


//Checks if the value at ith and jth index is valid
bool is_valid_mat(MATRIX matrix, short i, short j) {

	short row, col;
	short value = matrix.mat[i][j];

	// check column for validity
	for (row = 0; row < SIZE; row++) {
		if (matrix.mat[row][j] == 0)
			continue;

		if ((i != row) && 
				(matrix.mat[row][j] == value)) 
			return 0;
	}

	// check row for validity
	for (col = 0; col < SIZE; col++) {
		if (matrix.mat[i][col] == 0)
			continue;

		if (col != j && matrix.mat[i][col] == value)
			return 0;
	}

	// check the smaller grid for validity
	int l = (SIZE)/3;
	short igrid = (i / l) * l;
	short jgrid = (j / l) * l;
	for (row = igrid; row < igrid+l; row++) {
		for (col = jgrid; col < jgrid+l; col++) {
			if (matrix.mat[row][col] == 0)
				continue;

			if ((i != row) && (col != j) && (matrix.mat[row][col] == value)) {
				return false;
			}
		}
	}

	return true;
}

// free the give node from the linked list by deallocating its memory
void freeListNode(Node *node) {
	for (int i = 0; i < SIZE; i++) {
		free(node->mat.mat[i]);
		free(node->mat.fixed[i]);
	}
	free(node->mat.mat);
	free(node->mat.fixed);
	free(node);
}

//creates Node for the matrix and returns it
Node* createNode(MATRIX matrix, short i, short j){
	Node * curr = (Node *)malloc(sizeof(Node));
	int m;
	short x, y;

	/* allocate memory for new copy */

	curr->mat.mat = (short**)malloc(SIZE*sizeof(short*));
	for (m=0;m<SIZE;m++)
		curr->mat.mat[m] = (short*) malloc (SIZE * sizeof (short));

	curr->mat.fixed = (short**) malloc(SIZE * sizeof(short*));
	for (m=0;m<SIZE;m++)
		curr->mat.fixed[m] = (short*) malloc (SIZE * sizeof (short));


	//copy matrix
	for(x = 0; x < SIZE; x++){
		for(y = 0; y < SIZE; y++){
			curr->mat.mat[x][y] = matrix.mat[x][y];
			curr->mat.fixed[x][y] = matrix.fixed[x][y]; 
		}
	}

	curr->i = i;
	curr->j = j;
	curr->next = NULL;

	return curr;
}

//Link the nodes of the linked list and update its head and tail
void attachNode(Node* newNode){

	if(head == NULL){
		head = newNode;
		tail = newNode;
	} else {
		tail->next = newNode;
		tail = newNode;
	}
}

//removes an Node from the head of the linked list and returns it
Node* removeNode(){
	Node* result = NULL;
	if(head != NULL){
		result = head;
		head = result->next;
		if(head == NULL){
			tail = NULL;
		}
	}
	return result;
}

//Initialize all the possible board configurations for the given matrix
void buildEStack(MATRIX* matrix){

	short i = 0;
	short j = 0;

	if ((*matrix).fixed[i][j] == 1)
		findNextFreeCell(matrix, &i, &j);

	short num=0;

	Node* current = NULL;

	while(1) {
		((*matrix).mat[i][j])++;    

		//adding the matrix to the pool only if the value is is_valid_mat
		if (matrix->mat[i][j] <= SIZE && is_valid_mat(*matrix, i, j) == 1) 
		{
			Node* newPath = createNode(*matrix, i, j);
			attachNode(newPath);
			num++;
		} else if(matrix->mat[i][j] > SIZE) 
		{
			if(current != NULL)
			{
				freeListNode(current);
			}

			if(num >= SIZE)
			{
				break;
			}

			//exhausted all possible values for the given i,jth position
			Node* current = removeNode();

			if(current == NULL)
			{
				break;
			}

			matrix = &(current->mat);
			i = current->i;
			j = current->j;

			if(i == (SIZE-1) && j == (SIZE-1))
			{
				//Is a solution
				attachNode(current);
				break;
			}

			num--;

			findNextFreeCell(matrix, &i, &j);
		}
	}

	if(current != NULL)
	{
		freeListNode(current);
	}
}

//solve through all the possible matrices initialized in the pool as explicit stack 
short solvesudoku(MATRIX matrix) {

	head = NULL;
	tail = NULL;

	buildEStack(&matrix);

	short found = 0;
	short i, j;
	Node* current;
	int level;

#pragma omp parallel shared(found) private(i,j, current, level)
	{

#pragma omp critical (pool)
		current = removeNode();

		while(current != NULL && found == 0)
		{
			//cout<<" analysing current matrix "<<endl;

			MATRIX currMat = current->mat;

			i = current->i;
			j = current->j;

			findNextFreeCell(&currMat, &i, &j);

			level = 1;

			while (level > 0 && (i < SIZE) && found == 0) 
			{
				if (currMat.mat[i][j] < (SIZE)) 
				{    
					// increase cell value, and check if
					// new value is is_valid_mat
					currMat.mat[i][j]++;

					if (is_valid_mat(currMat, i, j) == 1) 
					{
						findNextFreeCell(&currMat, &i, &j);
						level++;
					}
				} else 
				{
					// goes back to the previous non-fixed cell
					currMat.mat[i][j] = 0;
					findPreFilledCell(&currMat, &i, &j);
					level--;
				} // end else

			} // end while

			if(i == (SIZE))
			{
				found = 1;
				solution = currMat;
				//break;
				continue;
			}

			free(current);

#pragma omp critical (pool)
			current = removeNode();

		}

	} 

	return found;
}

void findPreFilledCell(MATRIX* matrix, short* iPointer, short* jPointer){
	do {
		if (*jPointer == 0 && *iPointer > 0) {
			*jPointer = (SIZE) - 1;
			(*iPointer)--;
		} else
			(*jPointer)--;
	} while (*jPointer >= 0 && (*matrix).fixed[*iPointer][*jPointer] == 1);
}

void findNextFreeCell(MATRIX* matrix, short* iPointer, short* jPointer){

	do{
		if(*jPointer < (SIZE-1))
			(*jPointer)++;
		else {
			*jPointer = 0;
			(*iPointer)++;
		}
	} while (*iPointer < (SIZE) && (*matrix).fixed[*iPointer][*jPointer]);
}

int main(int argc, char* argv[])
{
	//read matrix;
	//todo: generate random sudoko matrix instead of reading it from a file
	MATRIX m = readSdkFromFile(argv[1]);
	std::clock_t start;
	start = std::clock();

	std::cout<<"\n\nThe given sudoku problem is \n"; 
	printMatrix(&m);
	//std::cout<<"\n\n";  
	short hasSolution = solvesudoku(m);
	if(hasSolution == 0){
		cout<<"\n\nNo solution has been found for the above problem!!\n";
		return 1;
	}

	//printing the solution to the standard output
	cout<<"\nThe solution to the above problem is \n";
	printMatrix(&solution);

	//todo: memory deallocation 
	/*
	   Node* node = head;
	   while (node != NULL) {
	   Node* next = node->next;
	//freeListNode(node);
	node = next;
	}
	*/
	std::clock_t end;
	end = std::clock();
	//cout<<" Total time taken for the computation is "<<end-start<<"s"<<endl;
	std::cout << "\n\nTotal time taken for the computation is: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms\n\n" << std::endl;

	return 0;
}

